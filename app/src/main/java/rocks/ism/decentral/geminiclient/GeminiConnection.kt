package rocks.ism.decentral.geminiclient

import android.graphics.Bitmap
import android.os.AsyncTask
import java.io.*
import java.net.URI
import javax.net.ssl.SSLSocket
import javax.net.ssl.SSLSocketFactory


class GeminiConnection {
    var currentURI : URI? = null
        private set
    suspend fun request(uri: URI): GeminiResult {
        currentURI = uri
        if(uri.scheme != "gemini") {
            return GeminiResult(this, GeminiConnectionState.FAILED_WRONG_URI_SCHEME)
        }

        var port = 1965
        if(uri.port != -1) {
            port = uri.port
        }

        val factory: SSLSocketFactory = SSLSocketFactory.getDefault() as SSLSocketFactory
        val socket: SSLSocket = factory.createSocket(uri.host, port) as SSLSocket
        socket.enabledProtocols = arrayOf("TLSv1.2")
        socket.startHandshake()

        val outWriter = PrintWriter(
            BufferedWriter(
                OutputStreamWriter(
                    socket.outputStream
                )
            )
        )

        outWriter.print(uri.toString() + "\r\n") //TODO: See if I can set <CR><LF> globally
        outWriter.flush()

        if (outWriter.checkError()) {
            return GeminiResult(this, GeminiConnectionState.FAILED_PRINTER_WRITER)
        }

        var inReader = BufferedReader(
            InputStreamReader(
                socket.inputStream
            )
        )

        var responseHeader: String = inReader.readLine()
        var responseBody: String = inReader.readText()

        inReader.close()
        outWriter.close()
        socket.close()

        return GeminiResult(this, GeminiConnectionState.SUCCESS, responseHeader, responseBody)
    }
}

enum class GeminiConnectionState {
    FAILED,
    FAILED_WRONG_URI_SCHEME,
    FAILED_PRINTER_WRITER,
    SUCCESS
}

class GeminiResult(val connection: GeminiConnection, val connectionState: GeminiConnectionState) {
    var header: String = ""
        private set

    var statusCode: Int = 0
        private set

    var meta: String = ""
        private set

    var body: String = ""
        private set

    constructor (connection: GeminiConnection, connectionState: GeminiConnectionState, header: String) : this(connection, connectionState) {
        this.header = header
        val headerParts = header.split("\\s+".toRegex(), 2)
        print(headerParts)
        this.statusCode = headerParts[0].toInt() //TODO: Check if not an int
        this.meta = headerParts[1]
    }
    constructor (connection: GeminiConnection, connectionState: GeminiConnectionState, header: String, body: String) : this(connection, connectionState, header) {
        this.body = body
    }

}