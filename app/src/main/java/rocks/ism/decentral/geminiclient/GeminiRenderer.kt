package rocks.ism.decentral.geminiclient

import android.text.Html
import android.text.SpannableString
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.method.LinkMovementMethod
import android.text.style.BulletSpan
import android.text.style.URLSpan
import android.widget.TextView
import android.widget.Toast
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.StringReader

class GeminiRenderer (val textView: TextView) {
    var preRenderMode = false
    fun render(connection: GeminiConnection, resultBody: String, mimetype: String) {
        if(mimetype == "text/gemini") {
            renderGemini(resultBody, connection)
        }
        else {
            renderPlaintext(resultBody)
        }
    }
    fun renderGemini(resultBody: String, connection: GeminiConnection) {
        val bodyReader = BufferedReader(StringReader(resultBody))
        preRenderMode = false
        textView.text = ""
        textView.movementMethod = LinkMovementMethod.getInstance()
        var line: String? = null
        while ({ line = bodyReader.readLine(); line }() != null) {
            line?.let {
                renderGeminiLine(it, connection)
            }
        }
    }

    fun renderGeminiLine (line: String, connection: GeminiConnection) {
        if(line.startsWith("=>")) {
            val linkParts = line.substring(2).trim().split("\\s+".toRegex(), 2)
            val linkUrl = connection.currentURI?.resolve(linkParts[0]).toString()
            var linkName = linkParts[0]
            if(linkParts.size > 1) {
                linkName = linkParts[1]
            }
            val spannable = SpannableString(linkName)
            spannable.setSpan(URLSpan(linkUrl), 0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
            textView.append(spannable)
        } else if(line.startsWith("*")) {
            val spannable = SpannableString(line.substring(1))
            spannable.setSpan(BulletSpan(), 0, spannable.length, SPAN_EXCLUSIVE_EXCLUSIVE)
            textView.append(spannable)
        }
        else {
            textView.append(line)
        }
        textView.append("\n")
    }

    fun renderPlaintext(resultBody: String) {
        textView.text = resultBody
    }

}