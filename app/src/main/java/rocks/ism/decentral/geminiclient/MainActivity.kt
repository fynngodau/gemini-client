package rocks.ism.decentral.geminiclient

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.net.URI


class MainActivity : AppCompatActivity() {

    private lateinit var connection : GeminiConnection
    private lateinit var renderer : GeminiRenderer
    private lateinit var responseHandler : GeminiResponseHandler

    private lateinit var backButton: Button
    private lateinit var goButton: Button
    private lateinit var uriField: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Find UI elements
        backButton = findViewById(R.id.back_button)
        goButton = findViewById(R.id.go_button)
        uriField = findViewById(R.id.gemini_uri)

        // Initialize Protocol objects
        connection = GeminiConnection()
        renderer = GeminiRenderer(findViewById(R.id.mainContentView))
        responseHandler = GeminiResponseHandler(renderer, applicationContext)

                // Create handlers
        goButton.setOnClickListener {
            goToPage(URI.create(uriField.text.toString()))

            // Hide keyboard
            uriField.clearFocus()
            val inputService = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputService.hideSoftInputFromWindow(uriField.windowToken, 0)
        }

        uriField.setOnKeyListener { _, keyCode, event ->
            if (event?.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
                goButton.callOnClick()

                true
            } else false
        }

        goToPage(URI.create(getString(R.string.default_url)))
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if(intent?.data != null) {
            intent?.data.toString().let{goToPage(URI.create(it))}
        }
    }

    fun goToPage(uri: URI) {
        uriField.setText(uri.toString())
        val deferredResult = GlobalScope.async { connection.request(uri) }
        responseHandler.handle(deferredResult)
    }
}
