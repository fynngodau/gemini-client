package rocks.ism.decentral.geminiclient

import android.content.Context
import android.content.Intent
import android.net.Uri
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GeminiResponseHandler (val renderer: GeminiRenderer, private val context: Context) {
    fun handle (deferredResult: Deferred<GeminiResult>) {
        CoroutineScope(Dispatchers.Main).launch {
            val result = deferredResult.await()
            if(result.header.startsWith("3")) {
                //TODO: Check stuff (like how many redirects were before this)
                val newUriString = result.connection.currentURI?.resolve(result.meta).toString()
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(newUriString), context, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            }
            else if(result.header.startsWith("2")){
                if(result.meta == "text/gemini") {
                    renderer.render(result.connection, result.body, "text/gemini")
                } else if (result.meta.startsWith("text/")) {
                    renderer.render(result.connection, result.body,"text/plain")
                }
            }
            else {
                renderer.render(result.connection, result.header,"text/plain")
            }

        }
    }
}