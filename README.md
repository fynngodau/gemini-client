# Gemini Client for Android

I read about the [Gemini Protocol](gemini://gemini.circumlunar.space/) some days ago and noticed that there was no native client for Android.
The protocol is really simple, so I decided to have a go at implementing it in a Language I didn't (and still don't) know (Kotlin). This client is far from complete, but it works to a certain degree and can serve as a client for Android until there is something real.
I am not good at programming, especially not with stuff that has a GUI and even less with adding networking functionality to it. I expect many flaws to be in my code and so should you, if you use it. Expect crashes on a regular basis.
If anyone finds this interesting, I might actually put a little more of my time into it, but until then, this will probably stay as-is.

If you want to download the Application without building it yourself, you can download the latest APK [from the releases](https://framagit.org/waweic/gemini-client/-/releases), although I do not recommend this.

## Stuff that works
* Requesting information via Gemini
* Viewing formatted text/gemini documents
  - Clickable links
  - Bullet points
* Viewing other text/* documents
* Open application when gemini:// link in other app / browser is clicked
* Open other application when a link is clicked that has a scheme the client can't handle
* Basic redirection
* Does not start a new Activity when you click anywhere (thanks to a much appreciated hint by [@utf8equalsX@fosstodon.org](https://fosstodon.org/@utf8equalsX))

## Stuff that doesn't work (aka wishlist for the future)
* A browsing history
* The "Back"-button
* Avoiding redirection loops
* Other character encodings than utf-8
* Connects to hosts with self signed certificates
* Certain SSL/TLS Versions on older Android versions (I probably can't fix this)
* Downloading anything that is not of type text/*
* Crashes when page is too big
* Caching for offline use
* Useful error messages
* Graceful error handling
* Markdown
* Headings in text/gemini documents
* Code quality / structure
* Gemini INPUT
* Client certificates
* Security

## Stuff that probably should be done differently
* Probably a lot